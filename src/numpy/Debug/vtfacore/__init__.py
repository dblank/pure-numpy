﻿"""
VTFA Core
=====

Provides
  1. UI Command and Toolbar Manipulation
  2. Console Message Logging
  3. Resource Loading

"""

__all__ = [ 'DocumentItems', 'ToolItems', 'MenuItems', 'ToolBars', 'ConsoleHost' ]

import sys
import clr

clr.AddReference("VTFACoreLibrary")
import Cascade.VTFA.Core
import Cascade.VTFA.Core.Models

import Runtime
from Runtime import *

MenuItems.Load('Configuration\\MenuItems.xaml')

from vtfacore import *