
import sys


if sys.platform == 'cli':
    import clr
    import math
    clr.AddReference("Numpy")
    import Cascade.VTFA.Python.Numpy
    Cascade.VTFA.Python.Numpy.umath.__init__()
    pi = math.pi
    e = math.e
    from Cascade.VTFA.Python.Numpy.umath import *

