﻿import sys
import clr
clr.AddReference("System.Core")
clr.AddReference("PresentationCore")
clr.AddReference("PresentationFramework")

import System
import System.Windows.Controls;
clr.ImportExtensions(System.Linq)
from System.Collections.Generic import List

from vtfacore import *