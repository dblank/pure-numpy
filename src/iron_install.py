# This file performs the installation step once the build is complete.
# Installation primarily involves copying the build results to the
# IronPython site-packages directory.

import os
import sys
import shutil
import tempfile
from os.path import dirname, isdir, isfile, join
from sys import argv

src_dir = argv[1]
sp_dir = argv[2]

def install():
	print "Installing from %s to %s ..." % (src_dir,sp_dir)

	dll_dir = sp_dir
	
	ignore_pys = ["setup.py", "iron_install.py"]
	ignore_libs = ["Microsoft.Scripting.dll",
					"Microsoft.Scripting.Metadata.dll",
					"Microsoft.Dynamic.dll",
					"IronPython.dll",
					"IronPython.Modules.dll"]


	os.chdir(src_dir)

	# Recursively walk the directory tree and copy all .py files into the
	# site-packages directory and all .dll files into DLLs.
	for root, dirs, files in os.walk('.'):
		for fn in files:
			#print "fn=%s" % (fn)
			rel_path = join(root, fn)
			if fn.endswith('.py') and fn not in ignore_pys:
				#print "abs_path = %s, relpath = %s" % ("", rel_path)
				dst_dir = dirname(join(sp_dir, rel_path))
				if not isdir(dst_dir):
						os.makedirs(dst_dir)

				# Rename the _clr.py files to remove the _clr suffix.
				# Only used for IronPython.
				if fn.endswith('_clr.py'):
					dst_file = join(dst_dir, fn[:-7] + ".py")
				else:
					dst_file = join(dst_dir, fn)

				print "Copy %s to %s" % (rel_path, dst_file)
				shutil.copy(rel_path, dst_file)
			elif fn.endswith('.dll') and fn not in ignore_libs:
				dst_file = join(dll_dir, fn)
				print rel_path
				if isfile(dst_file):
					# Rename existing file because it is probably in use
					# by the ipy command.
					tmp_dir = tempfile.mkdtemp()
					os.rename(dst_file, join(tmp_dir, fn))
				print "Copy %s to %s" % (rel_path, dst_file)
				shutil.copy(rel_path, dst_file)

if __name__ == '__main__':
    install()
