These files are versions of numpy in Pure Python (and some C/C++). The
code was collected in June 2014, but originates between 2000 and 2010.

src/psymeric was originally from Timothy Hochberg.

The rest of src and bin was originally from Mark DeArman. 

bin contains the build from src/numpy and src/libnarray.

Mark wrote:

```
#!text

You want to carefully go through npy_config.h in ndarray.  The issues
came from NPY_SIZEOF_ definitions.

You may also want to check all the NPY_HAVE definitions depending on
your C++ compiler and libraries.  There may be room for improvement by
fixing some of these.

I was going to dig into it more at a later time, especially how it
does the vectorization.

Since it seems to function fine for my needs currently, I have other
parts of my application that are more critical to finish first.

My build environment is VS2012 .NET 4.0 and Intel C++ 2013 SP1.
```
