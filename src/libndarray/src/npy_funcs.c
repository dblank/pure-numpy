
/*
 **************************************************************************
 **     This file was autogenerated from a template  DO NOT EDIT!!!!     **
 **     Changes should be made to the original source (.src) file        **
 **************************************************************************
 */

/* -*- c -*- */

#include "npy_defs.h"
#include "npy_math.h"
#include "npy_funcs.h"

/*
 * This file is for the definitions of the non-c99 functions used in ufuncs.
 * All the complex ufuncs are defined here along with a smattering of real and
 * object functions.
 */


/*
 *****************************************************************************
 **                           COMPLEX FUNCTIONS                             **
 *****************************************************************************
 */
typedef npy_longdouble longdouble;
#define cfloat npy_cfloat
#define cdouble npy_cdouble
#define clongdouble npy_clongdouble


/*
 * Don't pass structures between functions (only pointers) because how
 * structures are passed is compiler dependent and could cause segfaults if
 * umath_ufunc_object.inc is compiled with a different compiler than an
 * extension that makes use of the UFUNC API
 */


/*
 * Perform the operation  result := 1 + coef * x * result,
 * with real coefficient `coef`.
 */
#define SERIES_HORNER_TERMf(result, x, coef)                  \
    do {                                                        \
        npy_nc_prodf((result), (x), (result));                    \
        (result)->real *= (coef);                               \
        (result)->imag *= (coef);                               \
        npy_nc_sumf((result), &npy_nc_1f, (result));                \
    } while(0)

/* constants */
cfloat npy_nc_1f = {1., 0.};
cfloat npy_nc_halff = {0.5, 0.};
cfloat npy_nc_if = {0., 1.};
cfloat npy_nc_i2f = {0., 0.5};
/*
 *   cfloat npy_nc_mif = {0.0f, -1.0f};
 *   cfloat npy_nc_pi2f = {NPY_PI_2f., 0.0f};
 */


void
npy_nc_sumf(cfloat *a, cfloat *b, cfloat *r)
{
    r->real = a->real + b->real;
    r->imag = a->imag + b->imag;
    return;
}

void
npy_nc_difff(cfloat *a, cfloat *b, cfloat *r)
{
    r->real = a->real - b->real;
    r->imag = a->imag - b->imag;
    return;
}

void
npy_nc_negf(cfloat *a, cfloat *r)
{
    r->real = -a->real;
    r->imag = -a->imag;
    return;
}

void
npy_nc_prodf(cfloat *a, cfloat *b, cfloat *r)
{
    float ar=a->real, br=b->real, ai=a->imag, bi=b->imag;
    r->real = ar*br - ai*bi;
    r->imag = ar*bi + ai*br;
    return;
}

void
npy_nc_quotf(cfloat *a, cfloat *b, cfloat *r)
{

    float ar=a->real, br=b->real, ai=a->imag, bi=b->imag;
    float d = br*br + bi*bi;
    r->real = (ar*br + ai*bi)/d;
    r->imag = (ai*br - ar*bi)/d;
    return;
}

void
npy_nc_sqrtf(cfloat *x, cfloat *r)
{
    *r = npy_csqrtf(*x);
    return;
}

void
npy_nc_rintf(cfloat *x, cfloat *r)
{
    r->real = npy_rintf(x->real);
    r->imag = npy_rintf(x->imag);
}

void
npy_nc_logf(cfloat *x, cfloat *r)
{
    *r = npy_clogf(*x);
    return;
}

void
npy_nc_log1pf(cfloat *x, cfloat *r)
{
    float l = npy_hypotf(x->real + 1,x->imag);
    r->imag = npy_atan2f(x->imag, x->real + 1);
    r->real = npy_logf(l);
    return;
}

void
npy_nc_expf(cfloat *x, cfloat *r)
{
    *r = npy_cexpf(*x);
    return;
}

void
npy_nc_exp2f(cfloat *x, cfloat *r)
{
    cfloat a;
    a.real = x->real*NPY_LOGE2f;
    a.imag = x->imag*NPY_LOGE2f;
    npy_nc_expf(&a, r);
    return;
}

void
npy_nc_expm1f(cfloat *x, cfloat *r)
{
    float a = npy_expf(x->real);
    r->real = a*npy_cosf(x->imag) - 1.0f;
    r->imag = a*npy_sinf(x->imag);
    return;
}

void
npy_nc_powf(cfloat *a, cfloat *b, cfloat *r)
{
    npy_intp n;
    float ar = npy_crealf(*a);
    float br = npy_crealf(*b);
    float ai = npy_cimagf(*a);
    float bi = npy_cimagf(*b);

    if (br == 0. && bi == 0.) {
        *r = npy_cpackf(1., 0.);
        return;
    }
    if (ar == 0. && ai == 0.) {
        if (br > 0 && bi == 0) {
            *r = npy_cpackf(0., 0.);
        }
        else {
            /* NB: there are four complex zeros; c0 = (+-0, +-0), so that unlike
             *     for reals, c0**p, with `p` negative is in general
             *     ill-defined.
             *
             *     c0**z with z complex is also ill-defined.
             */
            *r = npy_cpackf(NPY_NAN, NPY_NAN);

            /* Raise invalid */
            ar = NPY_INFINITY;
            ar = ar - ar;
        }
        return;
    }
    if (bi == 0 && (n=(npy_intp)br) == br) {
        if (n == 1) {
            /* unroll: handle inf better */
            *r = npy_cpackf(ar, ai);
            return;
        }
        else if (n == 2) {
            /* unroll: handle inf better */
            npy_nc_prodf(a, a, r);
            return;
        }
        else if (n == 3) {
            /* unroll: handle inf better */
            npy_nc_prodf(a, a, r);
            npy_nc_prodf(a, r, r);
            return;
        }
        else if (n > -100 && n < 100) {
            cfloat p, aa;
            npy_intp mask = 1;
            if (n < 0) n = -n;
            aa = npy_nc_1f;
            p = npy_cpackf(ar, ai);
            while (1) {
                if (n & mask)
                    npy_nc_prodf(&aa,&p,&aa);
                mask <<= 1;
                if (n < mask || mask <= 0) break;
                npy_nc_prodf(&p,&p,&p);
            }
            *r = npy_cpackf(npy_crealf(aa), npy_cimagf(aa));
            if (br < 0) npy_nc_quotf(&npy_nc_1f, r, r);
            return;
        }
    }

    *r = npy_cpowf(*a, *b);
    return;
}


void
npy_nc_prodif(cfloat *x, cfloat *r)
{
    float xr = x->real;
    r->real = -x->imag;
    r->imag = xr;
    return;
}


void
npy_nc_acosf(cfloat *x, cfloat *r)
{
    /*
     * return npy_nc_neg(npy_nc_prodi(npy_nc_log(npy_nc_sum(x,npy_nc_prod(npy_nc_i,
     * npy_nc_sqrt(npy_nc_diff(npy_nc_1,npy_nc_prod(x,x))))))));
     */
    npy_nc_prodf(x,x,r);
    npy_nc_difff(&npy_nc_1f, r, r);
    npy_nc_sqrtf(r, r);
    npy_nc_prodif(r, r);
    npy_nc_sumf(x, r, r);
    npy_nc_logf(r, r);
    npy_nc_prodif(r, r);
    npy_nc_negf(r, r);
    return;
}

void
npy_nc_acoshf(cfloat *x, cfloat *r)
{
    /*
     * return npy_nc_log(npy_nc_sum(x,
     * npy_nc_prod(npy_nc_sqrt(npy_nc_sum(x,npy_nc_1)), npy_nc_sqrt(npy_nc_diff(x,npy_nc_1)))));
     */
    cfloat t;

    npy_nc_sumf(x, &npy_nc_1f, &t);
    npy_nc_sqrtf(&t, &t);
    npy_nc_difff(x, &npy_nc_1f, r);
    npy_nc_sqrtf(r, r);
    npy_nc_prodf(&t, r, r);
    npy_nc_sumf(x, r, r);
    npy_nc_logf(r, r);
    return;
}

void
npy_nc_asinf(cfloat *x, cfloat *r)
{
    /*
     * return npy_nc_neg(npy_nc_prodi(npy_nc_log(npy_nc_sum(npy_nc_prod(npy_nc_i,x),
     * npy_nc_sqrt(npy_nc_diff(npy_nc_1,npy_nc_prod(x,x)))))));
     */
    if (fabs(x->real) > 1e-3 || fabs(x->imag) > 1e-3) {
        cfloat a, *pa=&a;
        npy_nc_prodf(x, x, r);
        npy_nc_difff(&npy_nc_1f, r, r);
        npy_nc_sqrtf(r, r);
        npy_nc_prodif(x, pa);
        npy_nc_sumf(pa, r, r);
        npy_nc_logf(r, r);
        npy_nc_prodif(r, r);
        npy_nc_negf(r, r);
    }
    else {
        /*
         * Small arguments: series expansion, to avoid loss of precision
         * asin(x) = x [1 + (1/6) x^2 [1 + (9/20) x^2 [1 + ...]]]
         *
         * |x| < 1e-3 => |rel. error| < 1e-18 (f), 1e-24, 1e-36 (l)
         */
        cfloat x2;
        npy_nc_prodf(x, x, &x2);

        *r = npy_nc_1f;
#if 1 >= 3
        SERIES_HORNER_TERMf(r, &x2, 81.0F/110);
        SERIES_HORNER_TERMf(r, &x2, 49.0F/72);
#endif
#if 1 >= 2
        SERIES_HORNER_TERMf(r, &x2, 25.0F/42);
#endif
        SERIES_HORNER_TERMf(r, &x2, 9.0F/20);
        SERIES_HORNER_TERMf(r, &x2, 1.0F/6);
        npy_nc_prodf(r, x, r);
    }
    return;
}


void
npy_nc_asinhf(cfloat *x, cfloat *r)
{
    /*
     * return npy_nc_log(npy_nc_sum(npy_nc_sqrt(npy_nc_sum(npy_nc_1,npy_nc_prod(x,x))),x));
     */
    if (fabs(x->real) > 1e-3 || fabs(x->imag) > 1e-3) {
        npy_nc_prodf(x, x, r);
        npy_nc_sumf(&npy_nc_1f, r, r);
        npy_nc_sqrtf(r, r);
        npy_nc_sumf(r, x, r);
        npy_nc_logf(r, r);
    }
    else {
        /*
         * Small arguments: series expansion, to avoid loss of precision
         * asinh(x) = x [1 - (1/6) x^2 [1 - (9/20) x^2 [1 - ...]]]
         *
         * |x| < 1e-3 => |rel. error| < 1e-18 (f), 1e-24, 1e-36 (l)
         */
        cfloat x2;
        npy_nc_prodf(x, x, &x2);

        *r = npy_nc_1f;
#if 1 >= 3
        SERIES_HORNER_TERMf(r, &x2, -81.0F/110);
        SERIES_HORNER_TERMf(r, &x2, -49.0F/72);
#endif
#if 1 >= 2
        SERIES_HORNER_TERMf(r, &x2, -25.0F/42);
#endif
        SERIES_HORNER_TERMf(r, &x2, -9.0F/20);
        SERIES_HORNER_TERMf(r, &x2, -1.0F/6);
        npy_nc_prodf(r, x, r);
    }
    return;
}

void
npy_nc_atanf(cfloat *x, cfloat *r)
{
    /*
     * return npy_nc_prod(npy_nc_i2,npy_nc_log(npy_nc_quot(npy_nc_sum(npy_nc_i,x),npy_nc_diff(npy_nc_i,x))));
     */
    if (fabs(x->real) > 1e-3 || fabs(x->imag) > 1e-3) {
        cfloat a, *pa=&a;
        npy_nc_difff(&npy_nc_if, x, pa);
        npy_nc_sumf(&npy_nc_if, x, r);
        npy_nc_quotf(r, pa, r);
        npy_nc_logf(r,r);
        npy_nc_prodf(&npy_nc_i2f, r, r);
    }
    else {
        /*
         * Small arguments: series expansion, to avoid loss of precision
         * atan(x) = x [1 - (1/3) x^2 [1 - (3/5) x^2 [1 - ...]]]
         *
         * |x| < 1e-3 => |rel. error| < 1e-18 (f), 1e-24, 1e-36 (l)
         */
        cfloat x2;
        npy_nc_prodf(x, x, &x2);

        *r = npy_nc_1f;
#if 1 >= 3
        SERIES_HORNER_TERMf(r, &x2, -9.0F/11);
        SERIES_HORNER_TERMf(r, &x2, -7.0F/9);
#endif
#if 1 >= 2
        SERIES_HORNER_TERMf(r, &x2, -5.0F/7);
#endif
        SERIES_HORNER_TERMf(r, &x2, -3.0F/5);
        SERIES_HORNER_TERMf(r, &x2, -1.0F/3);
        npy_nc_prodf(r, x, r);
    }
    return;
}

void
npy_nc_atanhf(cfloat *x, cfloat *r)
{
    /*
     * return npy_nc_prod(npy_nc_half,npy_nc_log(npy_nc_quot(npy_nc_sum(npy_nc_1,x),npy_nc_diff(npy_nc_1,x))));
     */
    if (fabs(x->real) > 1e-3 || fabs(x->imag) > 1e-3) {
        cfloat a, *pa=&a;
        npy_nc_difff(&npy_nc_1f, x, r);
        npy_nc_sumf(&npy_nc_1f, x, pa);
        npy_nc_quotf(pa, r, r);
        npy_nc_logf(r, r);
        npy_nc_prodf(&npy_nc_halff, r, r);
    }
    else {
        /*
         * Small arguments: series expansion, to avoid loss of precision
         * atan(x) = x [1 + (1/3) x^2 [1 + (3/5) x^2 [1 + ...]]]
         *
         * |x| < 1e-3 => |rel. error| < 1e-18 (f), 1e-24, 1e-36 (l)
         */
        cfloat x2;
        npy_nc_prodf(x, x, &x2);

        *r = npy_nc_1f;
#if 1 >= 3
        SERIES_HORNER_TERMf(r, &x2, 9.0F/11);
        SERIES_HORNER_TERMf(r, &x2, 7.0F/9);
#endif
#if 1 >= 2
        SERIES_HORNER_TERMf(r, &x2, 5.0F/7);
#endif
        SERIES_HORNER_TERMf(r, &x2, 3.0F/5);
        SERIES_HORNER_TERMf(r, &x2, 1.0F/3);
        npy_nc_prodf(r, x, r);
    }
    return;
}

void
npy_nc_cosf(cfloat *x, cfloat *r)
{
    float xr=x->real, xi=x->imag;
    r->real = npy_cosf(xr)*npy_coshf(xi);
    r->imag = -npy_sinf(xr)*npy_sinhf(xi);
    return;
}

void
npy_nc_coshf(cfloat *x, cfloat *r)
{
    float xr=x->real, xi=x->imag;
    r->real = npy_cosf(xi)*npy_coshf(xr);
    r->imag = npy_sinf(xi)*npy_sinhf(xr);
    return;
}

void
npy_nc_log10f(cfloat *x, cfloat *r)
{
    npy_nc_logf(x, r);
    r->real *= NPY_LOG10Ef;
    r->imag *= NPY_LOG10Ef;
    return;
}

void
npy_nc_log2f(cfloat *x, cfloat *r)
{
    npy_nc_logf(x, r);
    r->real *= NPY_LOG2Ef;
    r->imag *= NPY_LOG2Ef;
    return;
}

void
npy_nc_sinf(cfloat *x, cfloat *r)
{
    float xr=x->real, xi=x->imag;
    r->real = npy_sinf(xr)*npy_coshf(xi);
    r->imag = npy_cosf(xr)*npy_sinhf(xi);
    return;
}

void
npy_nc_sinhf(cfloat *x, cfloat *r)
{
    float xr=x->real, xi=x->imag;
    r->real = npy_cosf(xi)*npy_sinhf(xr);
    r->imag = npy_sinf(xi)*npy_coshf(xr);
    return;
}

void
npy_nc_tanf(cfloat *x, cfloat *r)
{
    float sr,cr,shi,chi;
    float rs,is,rc,ic;
    float d;
    float xr=x->real, xi=x->imag;
    sr = npy_sinf(xr);
    cr = npy_cosf(xr);
    shi = npy_sinhf(xi);
    chi = npy_coshf(xi);
    rs = sr*chi;
    is = cr*shi;
    rc = cr*chi;
    ic = -sr*shi;
    d = rc*rc + ic*ic;
    r->real = (rs*rc+is*ic)/d;
    r->imag = (is*rc-rs*ic)/d;
    return;
}

void
npy_nc_tanhf(cfloat *x, cfloat *r)
{
    float si,ci,shr,chr;
    float rs,is,rc,ic;
    float d;
    float xr=x->real, xi=x->imag;
    si = npy_sinf(xi);
    ci = npy_cosf(xi);
    shr = npy_sinhf(xr);
    chr = npy_coshf(xr);
    rs = ci*shr;
    is = si*chr;
    rc = ci*chr;
    ic = si*shr;
    d = rc*rc + ic*ic;
    r->real = (rs*rc+is*ic)/d;
    r->imag = (is*rc-rs*ic)/d;
    return;
}

#undef SERIES_HORNER_TERMf

#undef longdouble



/*
 * Perform the operation  result := 1 + coef * x * result,
 * with real coefficient `coef`.
 */
#define SERIES_HORNER_TERM(result, x, coef)                  \
    do {                                                        \
        npy_nc_prod((result), (x), (result));                    \
        (result)->real *= (coef);                               \
        (result)->imag *= (coef);                               \
        npy_nc_sum((result), &npy_nc_1, (result));                \
    } while(0)

/* constants */
cdouble npy_nc_1 = {1., 0.};
cdouble npy_nc_half = {0.5, 0.};
cdouble npy_nc_i = {0., 1.};
cdouble npy_nc_i2 = {0., 0.5};
/*
 *   cdouble npy_nc_mi = {0.0, -1.0};
 *   cdouble npy_nc_pi2 = {NPY_PI_2., 0.0};
 */


void
npy_nc_sum(cdouble *a, cdouble *b, cdouble *r)
{
    r->real = a->real + b->real;
    r->imag = a->imag + b->imag;
    return;
}

void
npy_nc_diff(cdouble *a, cdouble *b, cdouble *r)
{
    r->real = a->real - b->real;
    r->imag = a->imag - b->imag;
    return;
}

void
npy_nc_neg(cdouble *a, cdouble *r)
{
    r->real = -a->real;
    r->imag = -a->imag;
    return;
}

void
npy_nc_prod(cdouble *a, cdouble *b, cdouble *r)
{
    double ar=a->real, br=b->real, ai=a->imag, bi=b->imag;
    r->real = ar*br - ai*bi;
    r->imag = ar*bi + ai*br;
    return;
}

void
npy_nc_quot(cdouble *a, cdouble *b, cdouble *r)
{

    double ar=a->real, br=b->real, ai=a->imag, bi=b->imag;
    double d = br*br + bi*bi;
    r->real = (ar*br + ai*bi)/d;
    r->imag = (ai*br - ar*bi)/d;
    return;
}

void
npy_nc_sqrt(cdouble *x, cdouble *r)
{
    *r = npy_csqrt(*x);
    return;
}

void
npy_nc_rint(cdouble *x, cdouble *r)
{
    r->real = npy_rint(x->real);
    r->imag = npy_rint(x->imag);
}

void
npy_nc_log(cdouble *x, cdouble *r)
{
    *r = npy_clog(*x);
    return;
}

void
npy_nc_log1p(cdouble *x, cdouble *r)
{
    double l = npy_hypot(x->real + 1,x->imag);
    r->imag = npy_atan2(x->imag, x->real + 1);
    r->real = npy_log(l);
    return;
}

void
npy_nc_exp(cdouble *x, cdouble *r)
{
    *r = npy_cexp(*x);
    return;
}

void
npy_nc_exp2(cdouble *x, cdouble *r)
{
    cdouble a;
    a.real = x->real*NPY_LOGE2;
    a.imag = x->imag*NPY_LOGE2;
    npy_nc_exp(&a, r);
    return;
}

void
npy_nc_expm1(cdouble *x, cdouble *r)
{
    double a = npy_exp(x->real);
    r->real = a*npy_cos(x->imag) - 1.0;
    r->imag = a*npy_sin(x->imag);
    return;
}

void
npy_nc_pow(cdouble *a, cdouble *b, cdouble *r)
{
    npy_intp n;
    double ar = npy_creal(*a);
    double br = npy_creal(*b);
    double ai = npy_cimag(*a);
    double bi = npy_cimag(*b);

    if (br == 0. && bi == 0.) {
        *r = npy_cpack(1., 0.);
        return;
    }
    if (ar == 0. && ai == 0.) {
        if (br > 0 && bi == 0) {
            *r = npy_cpack(0., 0.);
        }
        else {
            /* NB: there are four complex zeros; c0 = (+-0, +-0), so that unlike
             *     for reals, c0**p, with `p` negative is in general
             *     ill-defined.
             *
             *     c0**z with z complex is also ill-defined.
             */
            *r = npy_cpack(NPY_NAN, NPY_NAN);

            /* Raise invalid */
            ar = NPY_INFINITY;
            ar = ar - ar;
        }
        return;
    }
    if (bi == 0 && (n=(npy_intp)br) == br) {
        if (n == 1) {
            /* unroll: handle inf better */
            *r = npy_cpack(ar, ai);
            return;
        }
        else if (n == 2) {
            /* unroll: handle inf better */
            npy_nc_prod(a, a, r);
            return;
        }
        else if (n == 3) {
            /* unroll: handle inf better */
            npy_nc_prod(a, a, r);
            npy_nc_prod(a, r, r);
            return;
        }
        else if (n > -100 && n < 100) {
            cdouble p, aa;
            npy_intp mask = 1;
            if (n < 0) n = -n;
            aa = npy_nc_1;
            p = npy_cpack(ar, ai);
            while (1) {
                if (n & mask)
                    npy_nc_prod(&aa,&p,&aa);
                mask <<= 1;
                if (n < mask || mask <= 0) break;
                npy_nc_prod(&p,&p,&p);
            }
            *r = npy_cpack(npy_creal(aa), npy_cimag(aa));
            if (br < 0) npy_nc_quot(&npy_nc_1, r, r);
            return;
        }
    }

    *r = npy_cpow(*a, *b);
    return;
}


void
npy_nc_prodi(cdouble *x, cdouble *r)
{
    double xr = x->real;
    r->real = -x->imag;
    r->imag = xr;
    return;
}


void
npy_nc_acos(cdouble *x, cdouble *r)
{
    /*
     * return npy_nc_neg(npy_nc_prodi(npy_nc_log(npy_nc_sum(x,npy_nc_prod(npy_nc_i,
     * npy_nc_sqrt(npy_nc_diff(npy_nc_1,npy_nc_prod(x,x))))))));
     */
    npy_nc_prod(x,x,r);
    npy_nc_diff(&npy_nc_1, r, r);
    npy_nc_sqrt(r, r);
    npy_nc_prodi(r, r);
    npy_nc_sum(x, r, r);
    npy_nc_log(r, r);
    npy_nc_prodi(r, r);
    npy_nc_neg(r, r);
    return;
}

void
npy_nc_acosh(cdouble *x, cdouble *r)
{
    /*
     * return npy_nc_log(npy_nc_sum(x,
     * npy_nc_prod(npy_nc_sqrt(npy_nc_sum(x,npy_nc_1)), npy_nc_sqrt(npy_nc_diff(x,npy_nc_1)))));
     */
    cdouble t;

    npy_nc_sum(x, &npy_nc_1, &t);
    npy_nc_sqrt(&t, &t);
    npy_nc_diff(x, &npy_nc_1, r);
    npy_nc_sqrt(r, r);
    npy_nc_prod(&t, r, r);
    npy_nc_sum(x, r, r);
    npy_nc_log(r, r);
    return;
}

void
npy_nc_asin(cdouble *x, cdouble *r)
{
    /*
     * return npy_nc_neg(npy_nc_prodi(npy_nc_log(npy_nc_sum(npy_nc_prod(npy_nc_i,x),
     * npy_nc_sqrt(npy_nc_diff(npy_nc_1,npy_nc_prod(x,x)))))));
     */
    if (fabs(x->real) > 1e-3 || fabs(x->imag) > 1e-3) {
        cdouble a, *pa=&a;
        npy_nc_prod(x, x, r);
        npy_nc_diff(&npy_nc_1, r, r);
        npy_nc_sqrt(r, r);
        npy_nc_prodi(x, pa);
        npy_nc_sum(pa, r, r);
        npy_nc_log(r, r);
        npy_nc_prodi(r, r);
        npy_nc_neg(r, r);
    }
    else {
        /*
         * Small arguments: series expansion, to avoid loss of precision
         * asin(x) = x [1 + (1/6) x^2 [1 + (9/20) x^2 [1 + ...]]]
         *
         * |x| < 1e-3 => |rel. error| < 1e-18 (f), 1e-24, 1e-36 (l)
         */
        cdouble x2;
        npy_nc_prod(x, x, &x2);

        *r = npy_nc_1;
#if 2 >= 3
        SERIES_HORNER_TERM(r, &x2, 81.0/110);
        SERIES_HORNER_TERM(r, &x2, 49.0/72);
#endif
#if 2 >= 2
        SERIES_HORNER_TERM(r, &x2, 25.0/42);
#endif
        SERIES_HORNER_TERM(r, &x2, 9.0/20);
        SERIES_HORNER_TERM(r, &x2, 1.0/6);
        npy_nc_prod(r, x, r);
    }
    return;
}


void
npy_nc_asinh(cdouble *x, cdouble *r)
{
    /*
     * return npy_nc_log(npy_nc_sum(npy_nc_sqrt(npy_nc_sum(npy_nc_1,npy_nc_prod(x,x))),x));
     */
    if (fabs(x->real) > 1e-3 || fabs(x->imag) > 1e-3) {
        npy_nc_prod(x, x, r);
        npy_nc_sum(&npy_nc_1, r, r);
        npy_nc_sqrt(r, r);
        npy_nc_sum(r, x, r);
        npy_nc_log(r, r);
    }
    else {
        /*
         * Small arguments: series expansion, to avoid loss of precision
         * asinh(x) = x [1 - (1/6) x^2 [1 - (9/20) x^2 [1 - ...]]]
         *
         * |x| < 1e-3 => |rel. error| < 1e-18 (f), 1e-24, 1e-36 (l)
         */
        cdouble x2;
        npy_nc_prod(x, x, &x2);

        *r = npy_nc_1;
#if 2 >= 3
        SERIES_HORNER_TERM(r, &x2, -81.0/110);
        SERIES_HORNER_TERM(r, &x2, -49.0/72);
#endif
#if 2 >= 2
        SERIES_HORNER_TERM(r, &x2, -25.0/42);
#endif
        SERIES_HORNER_TERM(r, &x2, -9.0/20);
        SERIES_HORNER_TERM(r, &x2, -1.0/6);
        npy_nc_prod(r, x, r);
    }
    return;
}

void
npy_nc_atan(cdouble *x, cdouble *r)
{
    /*
     * return npy_nc_prod(npy_nc_i2,npy_nc_log(npy_nc_quot(npy_nc_sum(npy_nc_i,x),npy_nc_diff(npy_nc_i,x))));
     */
    if (fabs(x->real) > 1e-3 || fabs(x->imag) > 1e-3) {
        cdouble a, *pa=&a;
        npy_nc_diff(&npy_nc_i, x, pa);
        npy_nc_sum(&npy_nc_i, x, r);
        npy_nc_quot(r, pa, r);
        npy_nc_log(r,r);
        npy_nc_prod(&npy_nc_i2, r, r);
    }
    else {
        /*
         * Small arguments: series expansion, to avoid loss of precision
         * atan(x) = x [1 - (1/3) x^2 [1 - (3/5) x^2 [1 - ...]]]
         *
         * |x| < 1e-3 => |rel. error| < 1e-18 (f), 1e-24, 1e-36 (l)
         */
        cdouble x2;
        npy_nc_prod(x, x, &x2);

        *r = npy_nc_1;
#if 2 >= 3
        SERIES_HORNER_TERM(r, &x2, -9.0/11);
        SERIES_HORNER_TERM(r, &x2, -7.0/9);
#endif
#if 2 >= 2
        SERIES_HORNER_TERM(r, &x2, -5.0/7);
#endif
        SERIES_HORNER_TERM(r, &x2, -3.0/5);
        SERIES_HORNER_TERM(r, &x2, -1.0/3);
        npy_nc_prod(r, x, r);
    }
    return;
}

void
npy_nc_atanh(cdouble *x, cdouble *r)
{
    /*
     * return npy_nc_prod(npy_nc_half,npy_nc_log(npy_nc_quot(npy_nc_sum(npy_nc_1,x),npy_nc_diff(npy_nc_1,x))));
     */
    if (fabs(x->real) > 1e-3 || fabs(x->imag) > 1e-3) {
        cdouble a, *pa=&a;
        npy_nc_diff(&npy_nc_1, x, r);
        npy_nc_sum(&npy_nc_1, x, pa);
        npy_nc_quot(pa, r, r);
        npy_nc_log(r, r);
        npy_nc_prod(&npy_nc_half, r, r);
    }
    else {
        /*
         * Small arguments: series expansion, to avoid loss of precision
         * atan(x) = x [1 + (1/3) x^2 [1 + (3/5) x^2 [1 + ...]]]
         *
         * |x| < 1e-3 => |rel. error| < 1e-18 (f), 1e-24, 1e-36 (l)
         */
        cdouble x2;
        npy_nc_prod(x, x, &x2);

        *r = npy_nc_1;
#if 2 >= 3
        SERIES_HORNER_TERM(r, &x2, 9.0/11);
        SERIES_HORNER_TERM(r, &x2, 7.0/9);
#endif
#if 2 >= 2
        SERIES_HORNER_TERM(r, &x2, 5.0/7);
#endif
        SERIES_HORNER_TERM(r, &x2, 3.0/5);
        SERIES_HORNER_TERM(r, &x2, 1.0/3);
        npy_nc_prod(r, x, r);
    }
    return;
}

void
npy_nc_cos(cdouble *x, cdouble *r)
{
    double xr=x->real, xi=x->imag;
    r->real = npy_cos(xr)*npy_cosh(xi);
    r->imag = -npy_sin(xr)*npy_sinh(xi);
    return;
}

void
npy_nc_cosh(cdouble *x, cdouble *r)
{
    double xr=x->real, xi=x->imag;
    r->real = npy_cos(xi)*npy_cosh(xr);
    r->imag = npy_sin(xi)*npy_sinh(xr);
    return;
}

void
npy_nc_log10(cdouble *x, cdouble *r)
{
    npy_nc_log(x, r);
    r->real *= NPY_LOG10E;
    r->imag *= NPY_LOG10E;
    return;
}

void
npy_nc_log2(cdouble *x, cdouble *r)
{
    npy_nc_log(x, r);
    r->real *= NPY_LOG2E;
    r->imag *= NPY_LOG2E;
    return;
}

void
npy_nc_sin(cdouble *x, cdouble *r)
{
    double xr=x->real, xi=x->imag;
    r->real = npy_sin(xr)*npy_cosh(xi);
    r->imag = npy_cos(xr)*npy_sinh(xi);
    return;
}

void
npy_nc_sinh(cdouble *x, cdouble *r)
{
    double xr=x->real, xi=x->imag;
    r->real = npy_cos(xi)*npy_sinh(xr);
    r->imag = npy_sin(xi)*npy_cosh(xr);
    return;
}

void
npy_nc_tan(cdouble *x, cdouble *r)
{
    double sr,cr,shi,chi;
    double rs,is,rc,ic;
    double d;
    double xr=x->real, xi=x->imag;
    sr = npy_sin(xr);
    cr = npy_cos(xr);
    shi = npy_sinh(xi);
    chi = npy_cosh(xi);
    rs = sr*chi;
    is = cr*shi;
    rc = cr*chi;
    ic = -sr*shi;
    d = rc*rc + ic*ic;
    r->real = (rs*rc+is*ic)/d;
    r->imag = (is*rc-rs*ic)/d;
    return;
}

void
npy_nc_tanh(cdouble *x, cdouble *r)
{
    double si,ci,shr,chr;
    double rs,is,rc,ic;
    double d;
    double xr=x->real, xi=x->imag;
    si = npy_sin(xi);
    ci = npy_cos(xi);
    shr = npy_sinh(xr);
    chr = npy_cosh(xr);
    rs = ci*shr;
    is = si*chr;
    rc = ci*chr;
    ic = si*shr;
    d = rc*rc + ic*ic;
    r->real = (rs*rc+is*ic)/d;
    r->imag = (is*rc-rs*ic)/d;
    return;
}

#undef SERIES_HORNER_TERM

#undef longdouble



/*
 * Perform the operation  result := 1 + coef * x * result,
 * with real coefficient `coef`.
 */
#define SERIES_HORNER_TERMl(result, x, coef)                  \
    do {                                                        \
        npy_nc_prodl((result), (x), (result));                    \
        (result)->real *= (coef);                               \
        (result)->imag *= (coef);                               \
        npy_nc_suml((result), &npy_nc_1l, (result));                \
    } while(0)

/* constants */
clongdouble npy_nc_1l = {1., 0.};
clongdouble npy_nc_halfl = {0.5, 0.};
clongdouble npy_nc_il = {0., 1.};
clongdouble npy_nc_i2l = {0., 0.5};
/*
 *   clongdouble npy_nc_mil = {0.0l, -1.0l};
 *   clongdouble npy_nc_pi2l = {NPY_PI_2l., 0.0l};
 */


void
npy_nc_suml(clongdouble *a, clongdouble *b, clongdouble *r)
{
    r->real = a->real + b->real;
    r->imag = a->imag + b->imag;
    return;
}

void
npy_nc_diffl(clongdouble *a, clongdouble *b, clongdouble *r)
{
    r->real = a->real - b->real;
    r->imag = a->imag - b->imag;
    return;
}

void
npy_nc_negl(clongdouble *a, clongdouble *r)
{
    r->real = -a->real;
    r->imag = -a->imag;
    return;
}

void
npy_nc_prodl(clongdouble *a, clongdouble *b, clongdouble *r)
{
    longdouble ar=a->real, br=b->real, ai=a->imag, bi=b->imag;
    r->real = ar*br - ai*bi;
    r->imag = ar*bi + ai*br;
    return;
}

void
npy_nc_quotl(clongdouble *a, clongdouble *b, clongdouble *r)
{

    longdouble ar=a->real, br=b->real, ai=a->imag, bi=b->imag;
    longdouble d = br*br + bi*bi;
    r->real = (ar*br + ai*bi)/d;
    r->imag = (ai*br - ar*bi)/d;
    return;
}

void
npy_nc_sqrtl(clongdouble *x, clongdouble *r)
{
    *r = npy_csqrtl(*x);
    return;
}

void
npy_nc_rintl(clongdouble *x, clongdouble *r)
{
    r->real = npy_rintl(x->real);
    r->imag = npy_rintl(x->imag);
}

void
npy_nc_logl(clongdouble *x, clongdouble *r)
{
    *r = npy_clogl(*x);
    return;
}

void
npy_nc_log1pl(clongdouble *x, clongdouble *r)
{
    longdouble l = npy_hypotl(x->real + 1,x->imag);
    r->imag = npy_atan2l(x->imag, x->real + 1);
    r->real = npy_logl(l);
    return;
}

void
npy_nc_expl(clongdouble *x, clongdouble *r)
{
    *r = npy_cexpl(*x);
    return;
}

void
npy_nc_exp2l(clongdouble *x, clongdouble *r)
{
    clongdouble a;
    a.real = x->real*NPY_LOGE2l;
    a.imag = x->imag*NPY_LOGE2l;
    npy_nc_expl(&a, r);
    return;
}

void
npy_nc_expm1l(clongdouble *x, clongdouble *r)
{
    longdouble a = npy_expl(x->real);
    r->real = a*npy_cosl(x->imag) - 1.0l;
    r->imag = a*npy_sinl(x->imag);
    return;
}

void
npy_nc_powl(clongdouble *a, clongdouble *b, clongdouble *r)
{
    npy_intp n;
    longdouble ar = npy_creall(*a);
    longdouble br = npy_creall(*b);
    longdouble ai = npy_cimagl(*a);
    longdouble bi = npy_cimagl(*b);

    if (br == 0. && bi == 0.) {
        *r = npy_cpackl(1., 0.);
        return;
    }
    if (ar == 0. && ai == 0.) {
        if (br > 0 && bi == 0) {
            *r = npy_cpackl(0., 0.);
        }
        else {
            /* NB: there are four complex zeros; c0 = (+-0, +-0), so that unlike
             *     for reals, c0**p, with `p` negative is in general
             *     ill-defined.
             *
             *     c0**z with z complex is also ill-defined.
             */
            *r = npy_cpackl(NPY_NAN, NPY_NAN);

            /* Raise invalid */
            ar = NPY_INFINITY;
            ar = ar - ar;
        }
        return;
    }
    if (bi == 0 && (n=(npy_intp)br) == br) {
        if (n == 1) {
            /* unroll: handle inf better */
            *r = npy_cpackl(ar, ai);
            return;
        }
        else if (n == 2) {
            /* unroll: handle inf better */
            npy_nc_prodl(a, a, r);
            return;
        }
        else if (n == 3) {
            /* unroll: handle inf better */
            npy_nc_prodl(a, a, r);
            npy_nc_prodl(a, r, r);
            return;
        }
        else if (n > -100 && n < 100) {
            clongdouble p, aa;
            npy_intp mask = 1;
            if (n < 0) n = -n;
            aa = npy_nc_1l;
            p = npy_cpackl(ar, ai);
            while (1) {
                if (n & mask)
                    npy_nc_prodl(&aa,&p,&aa);
                mask <<= 1;
                if (n < mask || mask <= 0) break;
                npy_nc_prodl(&p,&p,&p);
            }
            *r = npy_cpackl(npy_creall(aa), npy_cimagl(aa));
            if (br < 0) npy_nc_quotl(&npy_nc_1l, r, r);
            return;
        }
    }

    *r = npy_cpowl(*a, *b);
    return;
}


void
npy_nc_prodil(clongdouble *x, clongdouble *r)
{
    longdouble xr = x->real;
    r->real = -x->imag;
    r->imag = xr;
    return;
}


void
npy_nc_acosl(clongdouble *x, clongdouble *r)
{
    /*
     * return npy_nc_neg(npy_nc_prodi(npy_nc_log(npy_nc_sum(x,npy_nc_prod(npy_nc_i,
     * npy_nc_sqrt(npy_nc_diff(npy_nc_1,npy_nc_prod(x,x))))))));
     */
    npy_nc_prodl(x,x,r);
    npy_nc_diffl(&npy_nc_1l, r, r);
    npy_nc_sqrtl(r, r);
    npy_nc_prodil(r, r);
    npy_nc_suml(x, r, r);
    npy_nc_logl(r, r);
    npy_nc_prodil(r, r);
    npy_nc_negl(r, r);
    return;
}

void
npy_nc_acoshl(clongdouble *x, clongdouble *r)
{
    /*
     * return npy_nc_log(npy_nc_sum(x,
     * npy_nc_prod(npy_nc_sqrt(npy_nc_sum(x,npy_nc_1)), npy_nc_sqrt(npy_nc_diff(x,npy_nc_1)))));
     */
    clongdouble t;

    npy_nc_suml(x, &npy_nc_1l, &t);
    npy_nc_sqrtl(&t, &t);
    npy_nc_diffl(x, &npy_nc_1l, r);
    npy_nc_sqrtl(r, r);
    npy_nc_prodl(&t, r, r);
    npy_nc_suml(x, r, r);
    npy_nc_logl(r, r);
    return;
}

void
npy_nc_asinl(clongdouble *x, clongdouble *r)
{
    /*
     * return npy_nc_neg(npy_nc_prodi(npy_nc_log(npy_nc_sum(npy_nc_prod(npy_nc_i,x),
     * npy_nc_sqrt(npy_nc_diff(npy_nc_1,npy_nc_prod(x,x)))))));
     */
    if (fabs(x->real) > 1e-3 || fabs(x->imag) > 1e-3) {
        clongdouble a, *pa=&a;
        npy_nc_prodl(x, x, r);
        npy_nc_diffl(&npy_nc_1l, r, r);
        npy_nc_sqrtl(r, r);
        npy_nc_prodil(x, pa);
        npy_nc_suml(pa, r, r);
        npy_nc_logl(r, r);
        npy_nc_prodil(r, r);
        npy_nc_negl(r, r);
    }
    else {
        /*
         * Small arguments: series expansion, to avoid loss of precision
         * asin(x) = x [1 + (1/6) x^2 [1 + (9/20) x^2 [1 + ...]]]
         *
         * |x| < 1e-3 => |rel. error| < 1e-18 (f), 1e-24, 1e-36 (l)
         */
        clongdouble x2;
        npy_nc_prodl(x, x, &x2);

        *r = npy_nc_1l;
#if 4 >= 3
        SERIES_HORNER_TERMl(r, &x2, 81.0L/110);
        SERIES_HORNER_TERMl(r, &x2, 49.0L/72);
#endif
#if 4 >= 2
        SERIES_HORNER_TERMl(r, &x2, 25.0L/42);
#endif
        SERIES_HORNER_TERMl(r, &x2, 9.0L/20);
        SERIES_HORNER_TERMl(r, &x2, 1.0L/6);
        npy_nc_prodl(r, x, r);
    }
    return;
}


void
npy_nc_asinhl(clongdouble *x, clongdouble *r)
{
    /*
     * return npy_nc_log(npy_nc_sum(npy_nc_sqrt(npy_nc_sum(npy_nc_1,npy_nc_prod(x,x))),x));
     */
    if (fabs(x->real) > 1e-3 || fabs(x->imag) > 1e-3) {
        npy_nc_prodl(x, x, r);
        npy_nc_suml(&npy_nc_1l, r, r);
        npy_nc_sqrtl(r, r);
        npy_nc_suml(r, x, r);
        npy_nc_logl(r, r);
    }
    else {
        /*
         * Small arguments: series expansion, to avoid loss of precision
         * asinh(x) = x [1 - (1/6) x^2 [1 - (9/20) x^2 [1 - ...]]]
         *
         * |x| < 1e-3 => |rel. error| < 1e-18 (f), 1e-24, 1e-36 (l)
         */
        clongdouble x2;
        npy_nc_prodl(x, x, &x2);

        *r = npy_nc_1l;
#if 4 >= 3
        SERIES_HORNER_TERMl(r, &x2, -81.0L/110);
        SERIES_HORNER_TERMl(r, &x2, -49.0L/72);
#endif
#if 4 >= 2
        SERIES_HORNER_TERMl(r, &x2, -25.0L/42);
#endif
        SERIES_HORNER_TERMl(r, &x2, -9.0L/20);
        SERIES_HORNER_TERMl(r, &x2, -1.0L/6);
        npy_nc_prodl(r, x, r);
    }
    return;
}

void
npy_nc_atanl(clongdouble *x, clongdouble *r)
{
    /*
     * return npy_nc_prod(npy_nc_i2,npy_nc_log(npy_nc_quot(npy_nc_sum(npy_nc_i,x),npy_nc_diff(npy_nc_i,x))));
     */
    if (fabs(x->real) > 1e-3 || fabs(x->imag) > 1e-3) {
        clongdouble a, *pa=&a;
        npy_nc_diffl(&npy_nc_il, x, pa);
        npy_nc_suml(&npy_nc_il, x, r);
        npy_nc_quotl(r, pa, r);
        npy_nc_logl(r,r);
        npy_nc_prodl(&npy_nc_i2l, r, r);
    }
    else {
        /*
         * Small arguments: series expansion, to avoid loss of precision
         * atan(x) = x [1 - (1/3) x^2 [1 - (3/5) x^2 [1 - ...]]]
         *
         * |x| < 1e-3 => |rel. error| < 1e-18 (f), 1e-24, 1e-36 (l)
         */
        clongdouble x2;
        npy_nc_prodl(x, x, &x2);

        *r = npy_nc_1l;
#if 4 >= 3
        SERIES_HORNER_TERMl(r, &x2, -9.0L/11);
        SERIES_HORNER_TERMl(r, &x2, -7.0L/9);
#endif
#if 4 >= 2
        SERIES_HORNER_TERMl(r, &x2, -5.0L/7);
#endif
        SERIES_HORNER_TERMl(r, &x2, -3.0L/5);
        SERIES_HORNER_TERMl(r, &x2, -1.0L/3);
        npy_nc_prodl(r, x, r);
    }
    return;
}

void
npy_nc_atanhl(clongdouble *x, clongdouble *r)
{
    /*
     * return npy_nc_prod(npy_nc_half,npy_nc_log(npy_nc_quot(npy_nc_sum(npy_nc_1,x),npy_nc_diff(npy_nc_1,x))));
     */
    if (fabs(x->real) > 1e-3 || fabs(x->imag) > 1e-3) {
        clongdouble a, *pa=&a;
        npy_nc_diffl(&npy_nc_1l, x, r);
        npy_nc_suml(&npy_nc_1l, x, pa);
        npy_nc_quotl(pa, r, r);
        npy_nc_logl(r, r);
        npy_nc_prodl(&npy_nc_halfl, r, r);
    }
    else {
        /*
         * Small arguments: series expansion, to avoid loss of precision
         * atan(x) = x [1 + (1/3) x^2 [1 + (3/5) x^2 [1 + ...]]]
         *
         * |x| < 1e-3 => |rel. error| < 1e-18 (f), 1e-24, 1e-36 (l)
         */
        clongdouble x2;
        npy_nc_prodl(x, x, &x2);

        *r = npy_nc_1l;
#if 4 >= 3
        SERIES_HORNER_TERMl(r, &x2, 9.0L/11);
        SERIES_HORNER_TERMl(r, &x2, 7.0L/9);
#endif
#if 4 >= 2
        SERIES_HORNER_TERMl(r, &x2, 5.0L/7);
#endif
        SERIES_HORNER_TERMl(r, &x2, 3.0L/5);
        SERIES_HORNER_TERMl(r, &x2, 1.0L/3);
        npy_nc_prodl(r, x, r);
    }
    return;
}

void
npy_nc_cosl(clongdouble *x, clongdouble *r)
{
    longdouble xr=x->real, xi=x->imag;
    r->real = npy_cosl(xr)*npy_coshl(xi);
    r->imag = -npy_sinl(xr)*npy_sinhl(xi);
    return;
}

void
npy_nc_coshl(clongdouble *x, clongdouble *r)
{
    longdouble xr=x->real, xi=x->imag;
    r->real = npy_cosl(xi)*npy_coshl(xr);
    r->imag = npy_sinl(xi)*npy_sinhl(xr);
    return;
}

void
npy_nc_log10l(clongdouble *x, clongdouble *r)
{
    npy_nc_logl(x, r);
    r->real *= NPY_LOG10El;
    r->imag *= NPY_LOG10El;
    return;
}

void
npy_nc_log2l(clongdouble *x, clongdouble *r)
{
    npy_nc_logl(x, r);
    r->real *= NPY_LOG2El;
    r->imag *= NPY_LOG2El;
    return;
}

void
npy_nc_sinl(clongdouble *x, clongdouble *r)
{
    longdouble xr=x->real, xi=x->imag;
    r->real = npy_sinl(xr)*npy_coshl(xi);
    r->imag = npy_cosl(xr)*npy_sinhl(xi);
    return;
}

void
npy_nc_sinhl(clongdouble *x, clongdouble *r)
{
    longdouble xr=x->real, xi=x->imag;
    r->real = npy_cosl(xi)*npy_sinhl(xr);
    r->imag = npy_sinl(xi)*npy_coshl(xr);
    return;
}

void
npy_nc_tanl(clongdouble *x, clongdouble *r)
{
    longdouble sr,cr,shi,chi;
    longdouble rs,is,rc,ic;
    longdouble d;
    longdouble xr=x->real, xi=x->imag;
    sr = npy_sinl(xr);
    cr = npy_cosl(xr);
    shi = npy_sinhl(xi);
    chi = npy_coshl(xi);
    rs = sr*chi;
    is = cr*shi;
    rc = cr*chi;
    ic = -sr*shi;
    d = rc*rc + ic*ic;
    r->real = (rs*rc+is*ic)/d;
    r->imag = (is*rc-rs*ic)/d;
    return;
}

void
npy_nc_tanhl(clongdouble *x, clongdouble *r)
{
    longdouble si,ci,shr,chr;
    longdouble rs,is,rc,ic;
    longdouble d;
    longdouble xr=x->real, xi=x->imag;
    si = npy_sinl(xi);
    ci = npy_cosl(xi);
    shr = npy_sinhl(xr);
    chr = npy_coshl(xr);
    rs = ci*shr;
    is = si*chr;
    rc = ci*chr;
    ic = si*shr;
    d = rc*rc + ic*ic;
    r->real = (rs*rc+is*ic)/d;
    r->imag = (is*rc-rs*ic)/d;
    return;
}

#undef SERIES_HORNER_TERMl

#undef longdouble



