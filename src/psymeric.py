from array import array as _array, ArrayType as _ArrayType
from types import SliceType as _SliceType

try:
    from numpy import bool_, int8, int16, float32, complex64
except:
    bool_, int8, int16, float32, complex64 = bool, int, int, float, complex


#-------- The Psymeric array type --------


class Array(object):

    __slots__ = ['dims', 'start', 'strides', 'data', '_type']

    def __init__(self, data, type=None):
        if data is None:
            return # Fast path for internal funcs
        dims = shape(data)
        data = _flatten(data)
        if type is None:
            type = typeof(data)
        strides = _shapetostrides(dims, type.width)
        self._init(type.coerce(data), 0, dims, strides, type)

    def _init(self, data, start, dims, strides, type):
        self.data = data
        self.start = start
        self.dims = dims
        self.strides = strides
        self._type = type
        #self.iscontig = _iscontiguous(dims, strides, self._type.width)
        return self
  
    def __add__(self, other):
        if not isinstance(other, Array): other = Array(other)
        return _ufunc_call("add", self, other)
    __radd__ = __add__
    def __iadd__(self, other):
        if not isinstance(other, Array): other = Array(other)
        return _ufunc_call("add", self, other, None, self)
    
    def __sub__(self, other):
        if not isinstance(other, Array): other = Array(other)
        return _ufunc_call("sub", self, other)
    def __rsub__(self, other):
        if not isinstance(other, Array): other = Array(other)
        return _ufunc_call("sub", other, self)
    def __isub__(self, other):
        if not isinstance(other, Array): other = Array(other)
        return _ufunc_call("sub", self, other, None, self)
    
    def __mul__(self, other):
        if not isinstance(other, Array): other = Array(other)
        return _ufunc_call("mul", self, other)
    __rmul__ = __mul__
    def __imul__(self, other):
        if not isinstance(other, Array): other = Array(other)
        return _ufunc_call("mul", self, other, None, self)
    
    def __div__(self, other):
        if not isinstance(other, Array): other = Array(other)
        return _ufunc_call("div", self, other)
    def __rdiv__(self, other):
        if not isinstance(other, Array): other = Array(other)
        return _ufunc_call("div", other, self)
    def __idiv__(self, other):
        if not isinstance(other, Array): other = Array(other)
        return _ufunc_call("div", self, other, None, self)

    def __gt__(self, other):
        if not isinstance(other, Array): other = Array(other)
        return _ufunc_call("gt", self, other)
    def __lt__(self, other):
        if not isinstance(other, Array): other = Array(other)
        return _ufunc_call("lt", self, other)
    def __eq__(self, other):
        if not isinstance(other, Array): other = Array(other)
        return _ufunc_call("eq", self, other)
    __req__ = __eq__
    
    def __getitem__(self, index):
        return self._indexed(index)._scalarize()

    def __setitem__(self, index, value):
        indexed = self._indexed(index)
        value = self._cast(value)
        dims = list(indexed.dims)
        strides = [0]*len(dims)
        excess = len(dims) - len(value.dims)
        if excess < 0:
            raise ValueError("object too deep for desired array")
        for i in range(excess):
            dims[i] = indexed.dims[i]
        for i in range(excess, len(dims)):
            dims[i] = value.dims[i-excess]
            strides[i] = value.strides[i-excess]
        value.dims = tuple(dims)
        value.strides = tuple(strides)
        value.iscontig = _iscontiguous(dims, strides, self._type.width)
        _arraycopy(value, indexed)
    
    def __str__(self):
        n = len(self.dims)
        if n == 0:
            # Special case rank zero arrays
            return self._formatscalar(self.data[self.start])
        if n == 1:
            l = [self._formatscalar(self[i]) for i in range(self.dims[0])]
            return '[' + ' '.join(l) + ']'
        else:
            # XXX need to add indentation and length control
            return '[' + '\n'.join([str(self[i]) for i in range(self.dims[0])]) + ']'
                
    def __repr__(self):
        return 'array(%s)' % str(self)

    def _cast(self, value):
        if isinstance(value, Array):
            return value
        return Array(value)

    def _decomposeslice(self, slice, length):
        step = slice.step
        if step is None:
            step = 1
        start = slice.start
        if start is None:
            if step > 0:
                start = 0
            else:
                start = length - 1
        stop = slice.stop
        if stop is None:
            if step > 0:
                stop = length
            else:
                stop = -1
        return start, stop, step

    def _formatscalar(self, x):
        return str(x)

    def _fixindex(self, index, length):
        if index < 0:
            index += length
        if 0 <= index < length:
            return index
        raise IndexError("illegal index")

    def _indexed(self, indices):
        if not isinstance(indices, tuple):
            indices = (indices,)
        ndims = len(self.dims)
        ninds = len(indices)
        # Compute the amount of stretch to assign to the first ellipsis
        stretch = ndims - ninds
        for index in indices:
            if index in (Ellipsis, None):
                stretch += 1
        # Compute the dimensions and strides
        start = self.start
        axis = 0
        dims = []
        strides = []
        for index in indices:
            if index is Ellipsis:
                for i in range(stretch):
                    dims.append(self.dims[axis])
                    strides.append(self.strides[axis])
                    axis += 1
                stretch = 0
            elif index is None:
                dims.append(1)
                strides.append(0)
            elif axis > ndims:
                raise IndexError("too many dimensions")
            elif isinstance(index, int):
                start += self._fixindex(index, self.dims[axis]) * self.strides[axis]
                axis += 1
            elif isinstance(index, _SliceType):
                sstart, sstop, sstep = self._decomposeslice(index, self.dims[axis])
		if sstep > 0:
		    dims.append(max(1 + (sstop - sstart - 1) / sstep, 0))
		else:
		    dims.append(max(1 - (sstart - sstop - 1) / sstep, 0))
		start += sstart * self.strides[axis]
		strides.append(sstep * self.strides[axis])
		axis += 1
	    else: 
		raise IndexError("illegal index")
	# Tack any extra indices onto the end.
        for i in range(axis, ndims):
            dims.append(self.dims[i])
            strides.append(self.strides[i])
	data = Array.__new__(Array)._init(self.data, start, tuple(dims), tuple(strides), self._type)
	return data

    def _scalarize(self):
        if not self.dims:
            return self._type.scalar(self.data, self.start)
        return self

    def typecode(self):
        return self._type.code

#-------- Types --------

_types = []

      

class Type(object):
    __slots__ = ["width", "level", "ebits", "code", "_zero", "name"]
    
    def __init__(self, name, level, ebits, code, width=1, opprefix="_basic_"):
        self.width = width
        self.level = level
        self.ebits = ebits
        self.code = code
        self._zero = self._getzero()
        self.name = name
        _types.append(self)

    def __repr__(self):
        return self.name
    __str__ = __repr__

    def _getzero(self):
        if self.code is None: # Go away when objectType appears
            return [0]
        else:
            return _array(self.code, [0])

    def coerce(self, data):
        return _array(self.code, list(data))

    def getop(self, opname, atype, btype):
        return getattr(self, opname)

    def scalar(self, data, start):
        return data[start]

    def zeros(self, shape):
        strides, size = _shapetostridesandsize(shape, self.width) 
        data = self._zero * size
        return Array.__new__(Array)._init(data, 0, shape, strides, self)

    def add(sub, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            rdata[rstart] = adata[astart] + bdata[bstart]
            astart += as_; bstart += bs; rstart += rs
    def sub(self, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            rdata[rstart] = adata[astart] - bdata[bstart]
            astart += as_; bstart += bs; rstart += rs
    def mul(self, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            rdata[rstart] = adata[astart] * bdata[bstart]
            astart += as_; bstart += bs; rstart += rs
    def div(self, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            rdata[rstart] = adata[astart] / bdata[bstart]
            astart += as_; bstart += bs; rstart += rs
            
    def eq(self, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            rdata[rstart] = adata[astart] == bdata[bstart]
            astart += as_; bstart += bs; rstart += rs
    def gt(self, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            rdata[rstart] = adata[astart] > bdata[bstart]
            astart += as_; bstart += bs; rstart += rs
    def lt(self, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            rdata[rstart] = adata[astart] < bdata[bstart]
            astart += as_; bstart += bs; rstart += rs

    def abs(self, adata, astart, as_, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            rdata[rstart] = abs(adata[astart])
            astart += as_; rstart += rs


class ComplexType(Type):
    __slots__ = ["width", "level", "ebits", "code", "_zero", "name"]

    def coerce(self, data):
        a = _array(self.code)
        for x in data:
            z = complex(x)
            a.append(z.real)
            a.append(z.imag)
        return a

    def getop(self, opname, atype, btype):
        aiscomplex = isinstance(atype, ComplexType)
        biscomplex = isinstance(btype, ComplexType)
        if aiscomplex and biscomplex:
            return getattr(self, opname+'cc')
        if aiscomplex:
            return getattr(self, opname+'cf')
        if biscomplex:
            return getattr(self, opname+'fc')
        return getattr(self, opname)

    def scalar(self, data, start):
        return data[start] + 1j*data[start+1]


    def addcc(sub, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            rdata[rstart] = adata[astart] + bdata[bstart]
            rdata[rstart+1] = adata[astart+1] + bdata[bstart+1]
            astart += as_; bstart += bs; rstart += rs
    def addcf(sub, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            rdata[rstart] = adata[astart] + bdata[bstart]
            rdata[rstart+1] = adata[astart+1]
            astart += as_; bstart += bs; rstart += rs
    def addfc(sub, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            rdata[rstart] = adata[astart] + bdata[bstart]
            rdata[rstart+1] = bdata[bstart+1]
            astart += as_; bstart += bs; rstart += rs

    def subcc(sub, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            rdata[rstart] = adata[astart] - bdata[bstart]
            rdata[rstart+1] = adata[astart+1] - bdata[bstart+1]
            astart += as_; bstart += bs; rstart += rs
    def subcf(sub, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            rdata[rstart] = adata[astart] - bdata[bstart]
            rdata[rstart+1] = adata[astart+1]
            astart += as_; bstart += bs; rstart += rs
    def subfc(sub, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            rdata[rstart] = adata[astart] - bdata[bstart]
            rdata[rstart+1] = -bdata[bstart+1]
            astart += as_; bstart += bs; rstart += rs

    def mulcc(sub, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            are = adata[astart]; aim = adata[astart+1]; bre = bdata[bstart]; bim = bdata[bstart+1]
            rdata[rstart] = are*bre - aim*bim
            rdata[rstart+1] = are*bim + aim*bre
            astart += as_; bstart += bs; rstart += rs
    def mulcf(sub, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            are = adata[astart]; aim = adata[astart+1]; bre = bdata[bstart]
            rdata[rstart] = are*bre
            rdata[rstart+1] = aim*bre
            astart += as_; bstart += bs; rstart += rs
    def mulfc(sub, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            are = adata[astart]; bre = bdata[bstart]; bim = bdata[bstart+1]
            rdata[rstart] = are*bre
            rdata[rstart+1] = are*bim
            astart += as_; bstart += bs; rstart += rs

    def divcc(sub, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:            
            are = adata[astart]; aim = adata[astart+1]; bre = bdata[bstart]; bim = bdata[bstart+1]
            if abs(bre) >= abs(bim):
                # XXX normally check for divide by zero here (bre = 0)
                ratio = bim / bre
                denom = bre + bim * ratio
                rre = (are + aim * ratio) / denom
                rim = (aim - are * ratio) / denom
            else:
                ratio = bre / bim;
		denom = bre * ratio + bim;
		rre = (are * ratio + aim) / denom
		rim = (aim * ratio - are) / denom
            rdata[rstart] = rre
            rdata[rstart+1] = rim
            astart += as_; bstart += bs; rstart += rs
    def divcf(sub, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            are = adata[astart]; aim = adata[astart+1]; bre = bdata[bstart]
            rdata[rstart] = are/bre
            rdata[rstart+1] = aim/bre
            astart += as_; bstart += bs; rstart += rs
    def divfc(sub, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            are = adata[astart]; bre = bdata[bstart]; bim = bdata[bstart+1]
            if abs(bre) >= abs(bim):
                # XXX normally check for divide by zero here (bre = 0)
                ratio = bim / bre
                denom = bre + bim * ratio
                rre = (are) / denom
                rim = (are * ratio) / denom
            else:
                ratio = bre / bim;
		denom = bre * ratio + bim;
		rre = (are * ratio) / denom
		rim = (are) / denom
            rdata[rstart] = rre
            rdata[rstart+1] = rim
            astart += as_; bstart += bs; rstart += rs

    def eqcc(sub, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            rdata[rstart] = (adata[astart] == bdata[bstart]) and (adata[astart+1] == bdata[bstart+1])
            astart += as_; bstart += bs; rstart += rs
    def eqcf(sub, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            rdata[rstart] = (adata[astart] == bdata[bstart]) and (adata[astart+1] == 0)
            astart += as_; bstart += bs; rstart += rs
    def eqfc(sub, adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart):
        while rstart != maxrstart:
            rdata[rstart] = (adata[astart] == bdata[bstart]) and (bdata[bstart+1] == 0)
            astart += as_; bstart += bs; rstart += rs

# XXX It would be better to define two functions here depending on whether numpy is available
# XXX so that we could elide half the tests if we don't have to worry about numpy.
def _typecode(data, level, ebits): # -> Utilities? XXX
    for x in data:
        for klass, dtype in ((int, Int32), (float, Float64), (complex, Complex64), 
                              (bool_, Int32), (int8, Int8), (int16, Int16), (float32, Float32), (complex64, Complex32)):   
            if isinstance(x, klass):
                # Return the "correct type" for arrays
                level = max(level, dtype.level)
                ebits = max(ebits, dtype.ebits)
                break
        else:
            # Not atomic, so assume a sequence
            level, ebits = _typecode(x, level, ebits)
    return level, ebits

# Atomic types in generic sequences must be int, float or complex

def commontype(atype, btype, cache={}): 
    key = (atype, btype)
    result = cache.get(key)
    if result is None:
        level = max(atype.level, btype.level)
        ebits = max(atype.ebits, btype.ebits)
        cache[key] = result = _lookuptype(level, ebits)
    return result

def typeof(data): 
    if hasattr(data, 'type'):
        return data.type()
    if hasattr(data, 'typecode'):
        return _typedict[data.typecode]
    # Return smallest possible typecodes for scalars.
    for klass, type in ((int, UInt8), (float, Float32), (complex, Complex32)):   
        if isinstance(data, klass):
            return type
    else:
        # Not atomic, so assume a sequence
        level, ebits =  _typecode(data, 0, 0)
        return _lookuptype(level, ebits)

def _lookuptype(level, ebits): 
    min_type = None
    for t in _types:
        if (t.level >= level) and (t.ebits >= ebits):
            if (min_type is None) or (t.level <= min_type.level) and (t.ebits <= min_type.ebits):
                min_type = t
    assert min_type is not None, "XXX need better error message"
    return min_type



#-------- Functions --------


def asarray(obj):
    if isinstance(obj, Array):
        return obj
    return Array(obj)


def shape(data):
    if hasattr(data, 'shape'):
        return data.shape
    if isinstance(data, _ArrayType):
        return (len(data),)
    try:
        n = len(data)
        if n > 0:
            s = shape(data[0])
            for x in data[1:]:
                assert shape(x) == s, "XXX NEED A BETTER ERROR MESSAGE"
            return (n,) + s
        return (0,)
    except:
        return ()




#-------- ufuncs --------


def _ufunc_call(opname, a, b, rtype=None, rarray=None):
    if rtype is None:
        rtype = commontype(a._type, b._type)
    astrides, bstrides, dims = _stretch(a, b)
    if rarray is None:
        rarray = rtype.zeros(dims)
    elif rarray.dims != dims:
        raise IndexError("indices don't match for inplace operation")
    op_inner = rtype.getop(opname, a._type, b._type)
    _ufunc_outer(a.data, a.start, astrides, b.data, b.start, bstrides, rarray.data, rarray.start, rarray.strides, dims, op_inner)
    return rarray


def _ufunc_outer(adata, astart, astrides, bdata, bstart, bstrides, rdata, rstart, rstrides, dims, op_inner):
    as_ = astrides[0]; astrides = astrides[1:]
    bs = bstrides[0]; bstrides = bstrides[1:]
    rs = rstrides[0]; rstrides = rstrides[1:]
    d = dims[0]; dims = dims[1:]
    maxrstart = rstart + d * rs
    if not dims:
        op_inner(adata, astart, as_, bdata, bstart, bs, rdata, rstart, rs, maxrstart)
    else:
        while rstart != maxrstart:
            _ufunc_outer(adata, astart, astrides, bdata, bstart, bstrides, rdata, rstart, rstrides, dims, op_inner)
            astart += as_; bstart += bs; rstart += rs


class BinaryUFunc(object):
    __slots__ = ["_opname", "_type"]
    
    def __init__(self, opname, type=None):
        self._opname = opname
        self._type = type
    
    def __call__(self, a, b):
        if not isinstance(a, Array): a = Array(a)
        if not isinstance(b, Array): b = Array(b)
        return _ufunc_call(self._opname, a, b, self._type)




# XXX generalize ScalarUnaryUFunc

class AllTrue(object):
    def __call__(self, a):
        a = asarray(a)
        return self._op_outer(a.data, a.start, a.strides,  a.dims, 0, len(a.strides) - 1, self.op_inner, 1)

    def _op_outer(self, adata, astart, astrides, dims, depth, max_depth, op_inner, value):
        as_ = astrides[depth]
        maxastart = astart + dims[depth] * astrides[depth]
        if depth == max_depth:
            value = op_inner(adata, astart, as_, maxastart, value)
        else:
            while astart != maxastart:
                value = self._op_outer(adata, astart, astrides, dims, depth+1, max_depth, op_inner, value)
                astart += as_
        return value
        
    def op_inner(self, adata, astart, as_, maxastart, value):
        if not value:
            return 0
        while astart != maxastart:
            if not adata[astart]:
                return 0
            astart += as_
        return 1
all_true = AllTrue()


def _stretch(a, b): # XXX tune! And check!
    if a.dims == b.dims:
        return a.strides, b.strides, a.dims
    lena = len(a.dims)
    lenb = len(b.dims)
    # Gaurantee that len(a) >= len(b) by switching if necessary
    if lena < lenb:
        a, b = b, a
        lena, lenb = lenb, lena
        switched = 1
    else:
        switched = 0
    # First extend b by padding the front of bdims with adims and the
    # front of astrides with zeros. This gaurantees that the arrays
    # allign up to excess.
    excess = lena - lenb
    astrides = list(a.strides)
    dims = list(a.dims)
    bdims = b.dims
    bstrides = [0]*lena
    bstrides[excess:] = list(b.strides)
    # Check that the arrays allign after excess. If the dimensions do
    # not match, then one of the two dimensions must be 1, in which
    # case it is stretched.
    for i in range(excess, lena):
        if dims[i] != bdims[i]: 
            if dims[i] == 1:
                dims[i] = bdims[i]
                astrides[i] = 0
                continue
            if bdims[i] == 1:
                bstrides[i] = 0
                continue
            raise ValueError("matrices are not aligned")
    if switched:
        astrides, bstrides = bstrides, astrides
    return tuple(astrides), tuple(bstrides), tuple(dims)


#-------- Utilities ---------


def _arraycopy(a, b):
    if a.dims != b.dims:
        raise ValueError("dimensions of arrays must match")
    # XXX Note that if a.width < b.width only a portion of b gets set (corresponding to a). I think that this is the right thing to do
    # XXX for complex arrays, but perhaps not in general. Making _arraycopy a little dumber and adding _copy call to individual classes might be better
    if a._type.width > b._type.width:
        raise ValueError
    for i in range(a._type.width):
        if len(a.dims) == 0:
            b.data[i] = a.data[i]
        else:
            _arraycopyhelper(a.data, a.start+i, a.strides, a._type.width, b.data, b.start+i, b.strides, b._type.width, b.dims, 0)

def _arraycopyhelper(adata, astart, astrides, awidth,
                     bdata, bstart, bstrides, bwidth, dims, depth):
    jmax = bstart + dims[depth] * bstrides[depth]
    if depth < len(dims) - 1:
        i = astart
        j = bstart
        while j != jmax:
            _arraycopyhelper(adata, i, astrides, awidth, bdata, bstrides, bwidth, dims, depth+1)
            i += astrides[depth]
            j += bstrides[depth]
    else:
        i = astart
        j = bstart
        while j != jmax:
            bdata[j] = adata[i]
            i += astrides[depth]
            j += bstrides[depth]



def _iscontiguous(dims, strides, width):
    return strides == _shapetostrides(dims, width)


_SS_CACHE_SIZE = 16
_ss_cache = {}
def _shapetostridesandsize(*key):
    result = _ss_cache.get(key)
    if result is None:
        if len(_ss_cache) >= _SS_CACHE_SIZE:
            _ss_cache.popitem()
        shape, width = key
        ndims = len(shape)
        strides = [0]*ndims
        s = width
        for i in range(ndims-1, -1, -1):
            strides[i] = s
            s *= shape[i]
        result = _ss_cache[key] = (tuple(strides), s)
    return result

def _shapetostrides(shape, width):
    return _shapetostridesandsize(shape, width)[0]

def _flatten_helper(seq, flat):
    if isinstance(seq, str):
        flat.append(seq)
    else:
        try:
            items = len(seq)
            for i in range(items):
                _flatten_helper(seq[i], flat)
        except TypeError:
            flat.append(seq)

def _flatten(seq):
    flat = []
    _flatten_helper(seq, flat)
    return flat

#--------- Make types and ufuncs ---------

NoType = Type("NoType", None, None, None)

UInt8 = Type("UInt8", 1, 1.5, 'B') 
Int8 = Type("Int8", 2, 1, 'b')
Int16 = Type("Int16", 2, 2, 'h')
Int32 = Type("Int32", 2, 4, 'i')
Float32 = Type("Float32", 3, 2, 'f')  # XXX 2 is a guess for effective bits (number of bits to represent largest float as an integer).
Float64 = Type("Float64", 3, 4, 'd')
Complex32 = ComplexType("Complex32", 4, 2, 'f', 2)
Complex64 = ComplexType("Complex64", 4, 4, 'd', 2)

    
add = BinaryUFunc("add")
subtract = BinaryUFunc("sub")
multiply = BinaryUFunc("mul")
divide = BinaryUFunc("div")
equal = BinaryUFunc("eq", Int8)
less = BinaryUFunc("lt", Int8)

#--------- Psycoize ---------

try:
    from psyco import bind, error as PsycoError
except:
    print "NOT USING PSYCO"
    def bind(*args, **kargs):
        pass
    PsycoError = TypeError
for thing in globals().values():
    try:
        bind(thing, rec=None)
    except (TypeError, PsycoError):
        pass
del thing, bind


#--------- Tests ---------


def _getval(data, shape, klass, atype, indices):
    a = klass(data, atype)
    if indices:
        a = eval("a[%s]" % indices)
    if shape is None:
        try:
            a = a[0]
        except:
            a = a[...]
##        print a, type(a)
    return a

def _getntype(atype):
    nptype = atype.code
    code = {'h' : 'i',
            'b' : 'b'}.get(nptype, nptype)
    if isinstance(atype, ComplexType):
        code = code.upper()
    return code

def _testcase(op, typeclasses, shapes, indices=(None, None), reps=3, checks=1):
    from time import clock
    import numpy, random
    RANGE = 10
    PREQUEL = 3
    dts = [0] * len(typeclasses)
    for i in range(reps):
        data = []
        for s in shapes:
            # Add one to avoid potential divide by zero in tests
            if s is None:
                value = RANGE * random.random() + 1
            else:
                value = (RANGE * numpy.random.random(s) + 1).tolist()
            data.append(value)
        for j in range(len(typeclasses)):
            atype, klass = typeclasses[j]
            a = _getval(data[0], shapes[0], klass, atype, indices[0])
            b = _getval(data[1], shapes[1], klass, atype, indices[1])
            t0 = clock()
            if op == '+':    c = a + b
            elif op == '-':  c = a - b
            elif op == '*':  c = a * b
            elif op == '/':  c = a / b
            elif op == '==': c = a==b
            elif op == '<' : c = a < b
            elif op == '+=': a += b; c = a
            elif op == '-=': a -= b; c = a
            elif op == '*=': a *= b; c = a
            elif op == '/=': a /= b; c = a
            else:
                raise ValueError("unknown op %s" % op)
            t1 = clock()
            if i >= PREQUEL:
                # Don't record timing for early runs
                dts[j] += (t1 - t0)
            if (i < checks) and (klass is Array):
                nptype = _getntype(atype)
                a0, b0 = a, b
                a = _getval(data[0], shapes[0], numpy.array, nptype, indices[0])
                b = _getval(data[1], shapes[1], numpy.array, nptype, indices[1])
                if (len(op) == 2) and (op != '=='):
                    exec("a %s b; check = a" % op)
                else:
                    exec("check = a %s b " % op)
                # XXX For numpy, Complex64 division differs at 1e-16 level from psymeric results.
                # XXX should either investigate and/or only use all close for the
                # XXX the complex division tests.
                # XXX Also should have psymeric all_close
                if not all_true(c == check):
                    print "Psymeric does not match numpy"
                    print c
                    print check
                    print c - check
                    #~ raise RuntimeError("Psymeric does not match numpy")
    if reps > PREQUEL:
        for j in range(len(typeclasses)):
            dts[j] /= (reps-PREQUEL)
    return dts


def _get_rep(shape, index):
    if shape is None:
        return "scalar"
    if index is None:
        return "array(%s)" % (shape,)
    return "array(%s)[%s]" % (shape, index)

def _test_ops(shapes, tnames, indices=(None,None), reps=8, checks=1):
    import numpy
    print "Testing with %s <op> %s" % (_get_rep(shapes[0], indices[0]), _get_rep(shapes[1], indices[1]))
    for tname in tnames:
        print tname
        psytype = globals()[tname]
        ntype = _getntype(psytype) 
        typeclasses = [(psytype, Array), (ntype, numpy.array)]
        ops = "+", "-", "*", "/", "==", 
        if tname[:4] != "Comp":
            ops += '<',
        if tname not in ("Int8", "Int16", "Float32", "Complex32") and (shapes[0] == shapes[1] or shapes[1] is None) \
           and (indices[0] == indices[1]):
            ops += '+=', '-=','*=', '/='
        for op in ops:
            print "   ", op,
            for x in _testcase(op, typeclasses, shapes, indices, reps, checks=checks):
                print "  ", x,
            print


def _test_indices(shape, *indices):
    from numpy.random import random
    for nptype, natype in [(Float64, 'd'), (Complex64, 'D')]:
        na = random(shape).astype(natype)
        pa = Array(na.tolist(), nptype)
        for x in indices:
            print "Testing: %s using %s" % (_get_rep(shape, x), nptype)
            naind = eval("na[%s]" % x)
            npind = eval("pa[%s]" % x)
            check = (npind == naind)
            if not all_true(check):
                raise ValueError("Indexed array does not match Numeric equivalent")

    
def test_all():
    import psyco
    #psyco.bind(numarray.array, 10)
    SHORT = False
    if SHORT:
        tnames = "Float64",# "Complex64"
    else:
        tnames = "Int8", "Int16", "Int32", "Float32", "Float64", "Complex32", "Complex64"
    sizes = [(3,3)]#, (300,100)]
    #_test_indices([100], ":", ":0", "5:100:2", "20:1:-1", "2:20:-2")
    if not SHORT:
        _test_indices([300, 300], ":", ":,:", ":0,-1:", "20:150", "30:2:-3,5:", "2,2:55:3")
    #_test_ops([(3000,), (3000,)], tnames)
    for n1, n2 in sizes:
        _test_ops([(n1,n2), (n1,n2)], tnames)
        #_test_ops([(n1,n2), (n1,n2)], tnames, ["::2,::2", "::2,::2"])
        if not SHORT:
            _test_ops([(n1,n2), None], tnames)
            _test_ops([None, (n1,n2)], tnames)
            _test_ops([(n1,n2), (n1,n2)], tnames, ["::2,::2", "1::2,::2"])
            _test_ops([(n1,1), (1,n2)], tnames)


if __name__ == "__main__":
    test_all()

