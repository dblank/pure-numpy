
import sys

if sys.platform == 'cli':
    import clr
    clr.AddReference("Numpy");
    from Cascade.VTFA.Python.Numpy import *
    ModuleMethods.__init__()
    from Cascade.VTFA.Python.Numpy.ModuleMethods import *
    import Cascade.VTFA.Python.Numpy.ModuleMethods as NDNMM
    typeinfo = NDNMM.typeinfo
    _flagdict = NDNMM._flagdict

